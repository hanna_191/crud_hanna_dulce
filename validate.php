<!-- Validacion del Login -->
<?php 
session_start();
if(isset($_SESSION['usuario'])){
    header('Location: index.php');
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $username = $_POST['username'];
    $password = $_POST['password'];
    require('conexion.php');
    $sql = $conexion->prepare("SELECT * FROM usuario WHERE username=:username AND pass=:password");
    $sql->execute(array(':username'=>$username, ':password'=>$password));

    $resultado = $sql->fetch();
    if($resultado){
        $_SESSION['username'] = $username;
        header('Location: admin.php');
    } else {
        header('Location: index.php');
    }
}

