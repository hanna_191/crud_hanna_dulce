<?php 

$user = 'root';
$pass = '';
$host = 'localhost';
$db = 'hanna_diana_crud';

try {
    $conexion = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Conexion realizada exitosamente";
} catch(PDOException $e) {
    echo "La conexion no se pudo realizar" .$e->getMessage();
}